package edu.sjsu.android.mortgagecalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    private EditText AmountBorrowed;
    private double InterestRate;
    private TextView IRTextView;
    private SeekBar InterestRateSB;
    private RadioGroup LoanTerm;
    private CheckBox Tax_Insurance;
    private TextView MonthlyPayment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AmountBorrowed = (EditText) findViewById(R.id.AmountBorrowed);
        IRTextView = (TextView) findViewById(R.id.IRTextView);
        InterestRateSB = (SeekBar) findViewById(R.id.InterestRateSB);
        LoanTerm = (RadioGroup) findViewById(R.id.LoanTerm);
        Tax_Insurance = (CheckBox) findViewById(R.id.Tax_Insurance);
        MonthlyPayment = (TextView) findViewById(R.id.MonthlyPayment);

        InterestRateSB.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser) {
                    InterestRate = InterestRateSB.getProgress() / 10.0;
                    IRTextView.setText(InterestRate + " % ");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public void onClick(View view) {
        String input = AmountBorrowed.getText().toString();

        //If String input is empty
        if(input.length() == 0) {
            Toast.makeText(this, "Amount borrowed cannot be empty", Toast.LENGTH_LONG).show();
            return;
        }
        //If String is not a number
        else if (!input.matches("\\d+(\\.\\d+)?")) {
            Toast.makeText(this,"Number is Invalid",Toast.LENGTH_LONG).show();
            return;
        }
        double Principal = Double.parseDouble(AmountBorrowed.getText().toString());
        double Interest = InterestRateSB.getProgress() / 10.0;
        double Years;

        if (((RadioButton) findViewById(R.id.fifteen)).isChecked())
            Years = 15;
        else if (((RadioButton) findViewById(R.id.twenty)).isChecked())
            Years = 20;
        else if (((RadioButton) findViewById(R.id.thirty)).isChecked())
            Years = 30;
        else {
            Toast.makeText(this, "Please select a loan term", Toast.LENGTH_LONG).show();
            return;
        }
        boolean isTax_Insurance = Tax_Insurance.isChecked();
        // Tax = P * 0.1 / (Years * 12) = P * 0.1 / N
        double Mortgage = LoanUtil.calculate(Principal, Interest, Years, isTax_Insurance);
        String output = String.format("$%.2f", Mortgage);
        MonthlyPayment.setText("Monthly Payment: " + output);


    }
}