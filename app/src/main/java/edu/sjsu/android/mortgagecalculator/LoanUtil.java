package edu.sjsu.android.mortgagecalculator;

public class LoanUtil {

    public static double calculate(double Principal, double Interest, double Year, boolean isTax_Insurance) {

        // Monthly interest (annual interest rate / 1200)
        double J = Interest / 1200;
        // selected year * 12 months (1Year)
        double N = Year * 12;
        double Tax = (isTax_Insurance) ? Principal * 0.1 / N : 0.0;

        if(Interest == 0.0) return isZero( Principal,  N,  Tax);
        else return notZero( Principal,  J,  N,  Tax);
    }

    // interest rate = 0%, monthly payment M = (P/N) + T
    private static double isZero(double Principal, double N, double T) {
        return Principal / N + T;
    }

    // interest rate > 0%, monthly payment M = (P * (J / (1 - Math.pow((1 + J), -N))) + T);
    private static double notZero(double Principal, double J, double N, double T) {
        return (Principal * (J / (1 - Math.pow((1 + J), -N))) + T);
    }
}
